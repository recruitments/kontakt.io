package io.kontakt.apps.temperature.generator;

import io.kontakt.apps.event.TemperatureReading;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.concurrent.atomic.AtomicInteger;

@Component
public class SimpleTemperatureGenerator implements TemperatureGenerator {

    private final Random random = new Random();
    private final AtomicInteger counter = new AtomicInteger(0);

    @Override
    public List<TemperatureReading> generate() {
        int counter = this.counter.getAndIncrement();
        return generate(counter, counter % 5 == 0, 100);
    }

    private List<TemperatureReading> generate(int counter, boolean withPossibleAnomalies, int howMany) {
        return TemperatureReading.multiBuilder()
                .withThermometerId(generateThermometerId(counter))
                .withRoomId(generateRoomId(counter))
                .withTimestampRangeInSeconds(1)
                .withTemperatures(generateMeasurements(withPossibleAnomalies, howMany))
                .build();
    }

    private List<Double> generateMeasurements(boolean withPossibleAnomalies, int howMany) {
        List<Double> measurements = new ArrayList<>(howMany);
        if (withPossibleAnomalies) {
            for (int i = 0; i < howMany; i++) {
                measurements.add(random.nextDouble(13.0d, 24.0d));
            }
        } else {
            for (int i = 0; i < howMany; i++) {
                measurements.add(random.nextDouble(19.0d, 21.0d));
            }
        }
        return measurements;
    }

    private static String generateThermometerId(int counter) {
        if (counter % 3 == 0) return "therm1";
        else if (counter % 3 == 1) return "therm2";
        else return "therm3";
    }

    private static String generateRoomId(int counter) {
        if (counter % 3 == 0) return "room1";
        else if (counter % 3 == 1) return "room2";
        else return "room1";
    }

}
