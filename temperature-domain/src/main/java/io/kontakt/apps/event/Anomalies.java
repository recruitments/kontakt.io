package io.kontakt.apps.event;

import java.util.List;

public record Anomalies(String thermometerId, List<AnomalyDetected> anomalies) {
    public boolean hasAnomalies() {
        return !anomalies.isEmpty();
    }
}
