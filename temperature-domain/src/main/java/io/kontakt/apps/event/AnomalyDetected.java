package io.kontakt.apps.event;

public record AnomalyDetected(AnomalyDetectedSource source, Anomaly anomaly) {

    public static AnomalyDetected ofTemperatureReading(AnomalyDetectedSource source, TemperatureReading temperatureReading) {
        return new AnomalyDetected(source, Anomaly.ofTemperatureReading(temperatureReading));
    }

}
