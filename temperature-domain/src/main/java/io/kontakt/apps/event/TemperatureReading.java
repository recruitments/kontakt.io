package io.kontakt.apps.event;

import java.time.Instant;
import java.time.temporal.ChronoUnit;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;

public record TemperatureReading(double temperature, String roomId, String thermometerId, Instant timestamp) {

    public static ThermometerIdBuilder multiBuilder() {
        return new TemperatureReadingMultiBuilder();
    }

    public interface ThermometerIdBuilder {
        RoomIdBuilder withThermometerId(String thermometerId);
    }

    public interface RoomIdBuilder {
        TimestampBuilder withRoomId(String roomId);
    }

    public interface TimestampBuilder {
        TemperatureBuilder withTimestampRangeInSeconds(Integer timestampRange);
    }

    public interface TemperatureBuilder {
        TemperatureReadingBuilder withTemperatures(Double ... temperatures);
        TemperatureReadingBuilder withTemperatures(List<Double> temperatures);
    }

    public interface TemperatureReadingBuilder {
        List<TemperatureReading> build();
    }

    public static class TemperatureReadingMultiBuilder implements
            ThermometerIdBuilder,
            RoomIdBuilder,
            TimestampBuilder,
            TemperatureBuilder,
            TemperatureReadingBuilder
    {

        private String thermometerId;
        private String roomId;
        private Integer timestampRange;
        private List<Double> temperatures;


        @Override
        public RoomIdBuilder withThermometerId(String thermometerId) {
            this.thermometerId = Optional.ofNullable(thermometerId).orElse("1");
            return this;
        }

        @Override
        public TimestampBuilder withRoomId(String roomId) {
            this.roomId = Optional.ofNullable(roomId).orElse("1");
            return this;
        }

        @Override
        public TemperatureBuilder withTimestampRangeInSeconds(Integer timestampRange) {
            this.timestampRange = Optional.ofNullable(timestampRange).orElse(20);
            return this;
        }

        @Override
        public TemperatureReadingBuilder withTemperatures(Double... temperatures) {
            this.temperatures = Arrays.asList(temperatures);
            return this;
        }

        @Override
        public TemperatureReadingBuilder withTemperatures(List<Double> temperatures) {
            this.temperatures = temperatures;
            return this;
        }

        @Override
        public List<TemperatureReading> build() {
            List<TemperatureReading> readings = new ArrayList<>();
            Instant time = Instant.now().minus(timestampRange, ChronoUnit.SECONDS);
            long timestampDiff = (timestampRange * 1000) / temperatures.size();
            for (Double temperature : temperatures) {
                readings.add(new TemperatureReading(temperature, roomId, thermometerId, time));
                time = time.plus(timestampDiff, ChronoUnit.MILLIS);
            }
            return readings;
        }
    }

}
