package io.kontakt.apps.event;

import java.time.Instant;

public record Anomaly(double temperature, String roomId, String thermometerId, Instant timestamp) {

    public static Anomaly ofTemperatureReading(TemperatureReading temperatureReading) {
        return new Anomaly(
                temperatureReading.temperature(),
                temperatureReading.roomId(),
                temperatureReading.thermometerId(),
                temperatureReading.timestamp()
        );
    }

}
