package io.kontakt.apps.event;

public record ThermometerAnomalies(String thermometerId, String roomId, Integer anomaliesDetected) {
}
