package io.kontakt.apps.anomaly.detector.detectors;

import io.kontakt.apps.event.AnomalyDetected;
import io.kontakt.apps.event.TemperatureReading;
import org.junit.jupiter.api.Test;

import java.util.Collection;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

class UnitBasedAnomalyDetectorTest {

    private final AnomalyDetectorStrategy service = new UnitBasedAnomalyDetector(AnomalyDetectorProperties.defaultConfig());

    @Test
    public void shouldDetectAnomaly() {
        //Given && When
        List<AnomalyDetected> anomalies = TemperatureReading.multiBuilder()
                .withThermometerId("1")
                .withRoomId("1")
                .withTimestampRangeInSeconds(10)
                .withTemperatures(20.1d, 21.2d, 20.3d, 19.1d, 20.1d, 19.2d, 20.1d, 18.1d, 19.4d, 20.1d, 27.1d, 23.1d)
                .build()
                .stream()
                .map(service::detectAnomalies)
                .flatMap(Collection::stream)
                .toList();

        //Then
        assertEquals(1, anomalies.size());
        assertEquals(27.1d, anomalies.get(0).anomaly().temperature());
    }

    @Test
    public void shouldNotDetectAnomalyWhenDoesNotExist() {
        //Given && When

        List<AnomalyDetected> anomalies = TemperatureReading.multiBuilder()
                .withThermometerId("1")
                .withRoomId("1")
                .withTimestampRangeInSeconds(10)
                .withTemperatures(20.1d, 21.2d, 20.3d, 19.1d, 20.1d, 19.2d, 20.1d, 18.1d, 19.4d, 20.1d, 19.5d, 23.1d)
                .build()
                .stream()
                .map(service::detectAnomalies)
                .flatMap(Collection::stream)
                .toList();

        //Then
        assertEquals(0, anomalies.size());
    }

    @Test
    public void shouldDetectAnomalyInOneThermometerWhenTwoProvided() {
        //Given && When

        List<AnomalyDetected> anomalies = TemperatureReading.multiBuilder()
                .withThermometerId("1")
                .withRoomId("1")
                .withTimestampRangeInSeconds(10)
                .withTemperatures(26d, 26d, 26d, 26d, 26d, 26d, 26d, 26d, 26d, 26d)
                .build()
                .stream()
                .map(service::detectAnomalies)
                .flatMap(Collection::stream)
                .toList();
        List<AnomalyDetected> anomalies2 = TemperatureReading.multiBuilder()
                .withThermometerId("2")
                .withRoomId("1")
                .withTimestampRangeInSeconds(10)
                .withTemperatures(20d, 20d, 20d, 20d, 20d, 20d, 20d, 20d, 20d, 26d)
                .build()
                .stream()
                .map(service::detectAnomalies)
                .flatMap(Collection::stream)
                .toList();


        //Then
        assertEquals(0, anomalies.size());
        assertEquals(1, anomalies2.size());
        assertEquals(26d, anomalies2.get(0).anomaly().temperature());
    }

}