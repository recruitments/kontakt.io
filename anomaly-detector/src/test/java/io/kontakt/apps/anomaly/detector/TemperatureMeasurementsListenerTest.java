package io.kontakt.apps.anomaly.detector;

import io.kontakt.apps.event.Anomalies;
import io.kontakt.apps.event.TemperatureReading;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Value;

import java.time.Duration;
import java.time.Instant;

public class TemperatureMeasurementsListenerTest extends AbstractIntegrationTest {

    @Value("${spring.cloud.stream.bindings.anomalyDetectorProcessor-in-0.destination}")
    private String inputTopic;

    @Value("${spring.cloud.stream.bindings.anomalyDetectorProcessor-out-0.destination}")
    private String outputTopic;

    @Test
    void testInOutFlow() {
        try (TestKafkaConsumer<Anomalies> consumer = new TestKafkaConsumer<>(
                kafkaContainer.getBootstrapServers(),
                outputTopic,
                Anomalies.class
        );
             TestKafkaProducer<TemperatureReading> producer = new TestKafkaProducer<>(
                     kafkaContainer.getBootstrapServers(),
                     inputTopic
             )) {
            TemperatureReading temperatureReading = new TemperatureReading(20d, "room", "thermometer", Instant.parse("2023-01-01T00:00:00.000Z"));
            producer.produce(temperatureReading.thermometerId(), temperatureReading);
            consumer.assertNoMoreRecords(Duration.ofSeconds(10));
        }
    }

    @Test
    void testAnomalyDetectedByUnitAndTime() {
        try (TestKafkaConsumer<Anomalies> consumer = new TestKafkaConsumer<>(
                kafkaContainer.getBootstrapServers(),
                outputTopic,
                Anomalies.class
        );
             TestKafkaProducer<TemperatureReading> producer = new TestKafkaProducer<>(
                     kafkaContainer.getBootstrapServers(),
                     inputTopic
             )) {
            TemperatureReading.multiBuilder()
                    .withThermometerId("1")
                    .withRoomId("1")
                    .withTimestampRangeInSeconds(12)
                    .withTemperatures(19d, 19d, 19d, 19d, 29d, 19d, 19d, 19d, 19d, 19d)
                    .build()
                    .forEach(temperature -> producer.produce(temperature.thermometerId(), temperature));

            consumer.drain((records) -> (records.size() == 1 && records.get(0).value().anomalies().size() == 2) || records.size() == 2, Duration.ofSeconds(10));
        }
    }
}
