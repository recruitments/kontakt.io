package io.kontakt.apps.anomaly.detector.detectors;

import io.kontakt.apps.event.AnomalyDetected;
import io.kontakt.apps.event.TemperatureReading;
import org.junit.jupiter.api.Test;

import java.util.Collection;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;

class TimeBasedAnomalyDetectorTest {

    private final AnomalyDetectorStrategy service = new TimeBasedAnomalyDetector(AnomalyDetectorProperties.defaultConfig());

    @Test
    public void shouldDetectAndRemoveAnomaly() {
        //Given && When
        List<AnomalyDetected> anomalies = TemperatureReading.multiBuilder()
                .withThermometerId("1")
                .withRoomId("1")
                .withTimestampRangeInSeconds(12)
                .withTemperatures(19.1d, 19.2d, 19.5d, 27.4d, 19.7d, 19.3d, 26.1d, 18.2d, 19.1d, 19.2d)
                .build()
                .stream()
                .map(service::detectAnomalies)
                .flatMap(Collection::stream)
                .toList();

        //Then
        assertEquals(2, anomalies.size());
        assertEquals(27.4d, anomalies.get(0).anomaly().temperature());
        assertEquals(26.1d, anomalies.get(1).anomaly().temperature());
    }

}