package io.kontakt.apps.anomaly.detector.detectors;

import io.kontakt.apps.event.AnomalyDetected;
import io.kontakt.apps.event.TemperatureReading;

import java.time.Instant;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import static io.kontakt.apps.event.AnomalyDetectedSource.TIME;

public class TimeBasedAnomalyDetector extends AnomalyDetectorStrategy {

    public TimeBasedAnomalyDetector(AnomalyDetectorProperties anomalyDetectorProperties) {
        super(anomalyDetectorProperties);
    }

    @Override
    protected List<AnomalyDetected> detectAndRemoveAnomalies(ThermometerTemperatureMeasurements thermometerMeasurements) {
        List<TemperatureReading> measurements = getTemperaturesToAnalyze(thermometerMeasurements.measurements());
        if (measurements.isEmpty())
            return new ArrayList<>();

        thermometerMeasurements.measurements().removeAll(measurements);
        double temperatureSumOfMeasurements = calculateSumForMeasurements(measurements);
        double mean = temperatureSumOfMeasurements / measurements.size();
        var anomalies = measurements
                .stream()
                .filter(measurement -> Math.abs(measurement.temperature() - mean) >= anomalyDetectorProperties.getTemperatureTolerance())
                .map(measurement -> AnomalyDetected.ofTemperatureReading(TIME, measurement))
                .collect(Collectors.toList());
        anomalies.addAll(detectAndRemoveAnomalies(thermometerMeasurements));
        return anomalies;
    }

    private List<TemperatureReading> getTemperaturesToAnalyze(List<TemperatureReading> measurements) {
        if (measurements.isEmpty()) return new ArrayList<>();
        Instant startTime = measurements.get(0).timestamp();
        List<TemperatureReading> measurementsToAnalyze = new ArrayList<>();
        for (TemperatureReading measurement : measurements) {
            if (isInTimeRange(startTime, measurement)) {
                measurementsToAnalyze.add(measurement);
            } else {
                return measurementsToAnalyze; //because now we have only 10 seconds period measurements to analyze
            }
        }
        return new ArrayList<>(); //because if we didn't return data from for loop that means we have not fully 10 seconds measurements period but could be 1s, 2s, 3s....
    }

    private boolean isInTimeRange(Instant startTime, TemperatureReading measurement) {
        return Math.abs(startTime.getEpochSecond() - measurement.timestamp().getEpochSecond()) < anomalyDetectorProperties.getAnalyzeTimeFrame();
    }

}
