package io.kontakt.apps.anomaly.detector.config;

import io.kontakt.apps.anomaly.detector.detectors.TimeBasedAnomalyDetector;
import io.kontakt.apps.anomaly.detector.detectors.AnomalyDetectorProperties;
import io.kontakt.apps.anomaly.detector.detectors.AnomalyDetectorStrategy;
import io.kontakt.apps.anomaly.detector.detectors.UnitBasedAnomalyDetector;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.util.Arrays;
import java.util.List;

@Configuration
public class AnomalyDetectorConfig {

    @Bean
    @Qualifier("timeBased")
    public AnomalyDetectorStrategy timeBasedAnomalyDetector(
            AnomalyDetectorProperties properties
    ) {
        return new TimeBasedAnomalyDetector(properties);
    }

    @Bean
    @Qualifier("unitBased")
    public AnomalyDetectorStrategy unitBasedAnomalyDetector(
            AnomalyDetectorProperties properties
    ) {
        return new UnitBasedAnomalyDetector(properties);
    }

    @Bean
    @Qualifier("anomalyDetectors")
    public List<AnomalyDetectorStrategy> anomalyDetectors(
            @Qualifier("unitBased") AnomalyDetectorStrategy unitBased,
            @Qualifier("timeBased") AnomalyDetectorStrategy timeBased
    ) {
        return Arrays.asList(unitBased, timeBased);
    }

}
