package io.kontakt.apps.anomaly.detector.detectors;

import io.kontakt.apps.event.TemperatureReading;

import java.util.ArrayList;
import java.util.List;

public record ThermometerTemperatureMeasurements(
        String thermometerId,
        List<TemperatureReading> measurements
) {

    public static ThermometerTemperatureMeasurements ofThermometerId(String thermometerId) {
        return new ThermometerTemperatureMeasurements(thermometerId, new ArrayList<>());
    }

}
