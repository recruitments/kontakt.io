package io.kontakt.apps.anomaly.detector.detectors;

import io.kontakt.apps.event.AnomalyDetected;
import io.kontakt.apps.event.TemperatureReading;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import static io.kontakt.apps.event.AnomalyDetectedSource.UNIT;

public class UnitBasedAnomalyDetector extends AnomalyDetectorStrategy {

    public UnitBasedAnomalyDetector(AnomalyDetectorProperties anomalyDetectorProperties) {
        super(anomalyDetectorProperties);
    }

    @Override
    protected List<AnomalyDetected> detectAndRemoveAnomalies(ThermometerTemperatureMeasurements thermometerMeasurements) {
        List<TemperatureReading> measurements = getTemperaturesToAnalyze(thermometerMeasurements.measurements());
        if ( measurements.isEmpty() )
            return new ArrayList<>();

        List<AnomalyDetected> anomalies = detectAnomalies(thermometerMeasurements, measurements);
        if (anomalies.isEmpty() && !thermometerMeasurements.measurements().isEmpty())
            thermometerMeasurements.measurements().remove(0);
        boolean shouldStillDetectAnomalies = thermometerMeasurements.measurements().size() >= anomalyDetectorProperties.getAnalyzeUnits();
        if ( shouldStillDetectAnomalies )
            anomalies.addAll(detectAndRemoveAnomalies(thermometerMeasurements));
        return anomalies;
    }

    private List<AnomalyDetected> detectAnomalies(ThermometerTemperatureMeasurements thermometerTemperatureMeasurements,  List<TemperatureReading> measurements) {
        int temperatureTolerance = anomalyDetectorProperties.getTemperatureTolerance();
        double sumOfTemperatures = calculateSumForMeasurements(measurements);
        return measurements
                .stream()
                .filter(measurement -> {
                    double meanOfRemaining = (sumOfTemperatures - measurement.temperature()) / (measurements.size() - 1);
                    boolean isAnomaly = Math.abs(meanOfRemaining - measurement.temperature()) >= temperatureTolerance;
                    if (isAnomaly) thermometerTemperatureMeasurements.measurements().remove(measurement);
                    return isAnomaly;
                })
                .map(measurement -> AnomalyDetected.ofTemperatureReading(UNIT, measurement))
                .collect(Collectors.toList());
    }

    private List<TemperatureReading> getTemperaturesToAnalyze(List<TemperatureReading> measurements) {
        int units = anomalyDetectorProperties.getAnalyzeUnits();
        if ( measurements.size() < units ) return new ArrayList<>();
        return measurements.stream().limit(anomalyDetectorProperties.getAnalyzeUnits()).toList();
    }

}
