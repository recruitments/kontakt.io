package io.kontakt.apps.anomaly.detector.detectors;

import lombok.Builder;
import lombok.ToString;
import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.boot.context.properties.ConstructorBinding;

import java.util.Optional;

@Slf4j
@Builder
@ToString
@ConfigurationProperties(prefix = "anomalies.detector")
@ConstructorBinding
public class AnomalyDetectorProperties {

    private static final Integer DEFAULT_TEMPERATURE_TOLERANCE = 5;
    private static final Integer DEFAULT_ANALYZE_UNITS = 10;
    private static final Integer DEFAULT_ANALYZE_TIME_FRAME_SECONDS = 10;

    private final Integer temperatureTolerance;
    private final Integer analyzeUnits;
    private final Integer analyzeTimeFrame;

    public static AnomalyDetectorProperties defaultConfig() {
        return new AnomalyDetectorProperties(null, null, null);
    }

    public AnomalyDetectorProperties(
            Integer temperatureTolerance,
            Integer analyzeUnits,
            Integer analyzeTimeFrame
    ) {
        this.temperatureTolerance = Optional.ofNullable(temperatureTolerance).orElse(DEFAULT_TEMPERATURE_TOLERANCE);
        this.analyzeUnits = Optional.ofNullable(analyzeUnits).orElse(DEFAULT_ANALYZE_UNITS);
        this.analyzeTimeFrame = Optional.ofNullable(analyzeTimeFrame).orElse(DEFAULT_ANALYZE_TIME_FRAME_SECONDS);
        logConfig();
    }

    public Integer getTemperatureTolerance() {
        return temperatureTolerance;
    }

    public Integer getAnalyzeUnits() {
        return analyzeUnits;
    }

    public Integer getAnalyzeTimeFrame() {
        return analyzeTimeFrame;
    }

    private void logConfig() {
        log.info("Using anomalies detector configuration: {}", this);
    }

}
