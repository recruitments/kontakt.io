package io.kontakt.apps.anomaly.detector;

import io.kontakt.apps.event.Anomalies;
import io.kontakt.apps.event.TemperatureReading;
import lombok.extern.slf4j.Slf4j;
import org.apache.kafka.streams.kstream.KStream;

import java.util.function.Function;

@Slf4j
public class TemperatureMeasurementsListener implements Function<KStream<String, TemperatureReading>, KStream<String, Anomalies>> {

    private final AnomalyDetector anomalyDetector;

    public TemperatureMeasurementsListener(AnomalyDetector anomalyDetector) {
        this.anomalyDetector = anomalyDetector;
    }

    @Override
    public KStream<String, Anomalies> apply(KStream<String, TemperatureReading> events) {
        return events
                .mapValues(anomalyDetector::apply)
                .filter((s, anomalies) -> anomalies.hasAnomalies())
                .mapValues((s, anomalies) -> anomalies)
                .selectKey((s, anomaly) -> anomaly.thermometerId());
    }
}
