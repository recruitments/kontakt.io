package io.kontakt.apps.anomaly.detector;

import io.kontakt.apps.anomaly.detector.detectors.AnomalyDetectorStrategy;
import io.kontakt.apps.event.Anomalies;
import io.kontakt.apps.event.TemperatureReading;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Collection;
import java.util.List;

@Slf4j
@AllArgsConstructor
@Service
public class AnomalyDetectorDispatcher implements AnomalyDetector {

    @Autowired
    private List<AnomalyDetectorStrategy> anomalyDetectors;

    @Override
    public Anomalies apply(TemperatureReading temperatureReading) {
        if (log.isDebugEnabled())
            log.debug("Processing thermometer measurement {}", temperatureReading);
        var allAnomaliesForThermometer = anomalyDetectors
                .stream()
                .map(detector -> detector.detectAnomalies(temperatureReading))
                .flatMap(Collection::stream)
                .toList();
        if (!allAnomaliesForThermometer.isEmpty()) {
            log.info("Detected anomalies: {}", allAnomaliesForThermometer);
        }
        return new Anomalies(temperatureReading.thermometerId(), allAnomaliesForThermometer);
    }
}
