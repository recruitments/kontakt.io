package io.kontakt.apps.anomaly.detector.detectors;

import io.kontakt.apps.event.AnomalyDetected;
import io.kontakt.apps.event.TemperatureReading;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.*;

@RequiredArgsConstructor
public abstract class AnomalyDetectorStrategy {

    private final Map<String, ThermometerTemperatureMeasurements> thermometersTemperatureMeasurements = Collections.synchronizedMap(new HashMap<>());

    @Autowired
    protected final AnomalyDetectorProperties anomalyDetectorProperties;

    synchronized public List<AnomalyDetected> detectAnomalies(TemperatureReading measurement) {
        var thermometerTemperatureMeasurements = thermometersTemperatureMeasurements
                .getOrDefault(
                        measurement.thermometerId(),
                        ThermometerTemperatureMeasurements.ofThermometerId(measurement.thermometerId())
                );
        addMeasurementSorted(measurement, thermometerTemperatureMeasurements.measurements());
        thermometersTemperatureMeasurements.put(measurement.thermometerId(), thermometerTemperatureMeasurements);
        return detectAndRemoveAnomalies(thermometerTemperatureMeasurements);
    }

    private void addMeasurementSorted(TemperatureReading measurement, List<TemperatureReading> measurements) {
        boolean inserted = false;
        for (int i = 0; i < measurements.size(); i++) {
            if (measurement.timestamp().isBefore(measurements.get(i).timestamp())) {
                measurements.add(i, measurement);
                inserted = true;
                break;
            }
        }
        if (!inserted) {
            measurements.add(measurement);
        }
    }

    protected abstract List<AnomalyDetected> detectAndRemoveAnomalies(ThermometerTemperatureMeasurements thermometerMeasurements);

    protected double calculateSumForMeasurements(List<TemperatureReading> measurements) {
        return measurements
                .stream()
                .map(TemperatureReading::temperature)
                .reduce(0d, Double::sum);
    }

}
