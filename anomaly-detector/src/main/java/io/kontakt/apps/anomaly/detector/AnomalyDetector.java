package io.kontakt.apps.anomaly.detector;

import io.kontakt.apps.event.Anomalies;
import io.kontakt.apps.event.TemperatureReading;

import java.util.function.Function;

public interface AnomalyDetector extends Function<TemperatureReading, Anomalies> {

}
