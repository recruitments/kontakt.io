package io.kontakt.apps.temperature.analytics.api;

import io.kontakt.apps.event.AnomalyDetectedSource;
import io.kontakt.apps.temperature.analytics.api.storage.entity.AnomalyEntity;
import io.kontakt.apps.temperature.analytics.api.storage.entity.MeasurementEntity;
import io.kontakt.apps.temperature.analytics.api.storage.entity.ThermometerEntity;

import java.time.Instant;
import java.time.temporal.ChronoUnit;
import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static io.kontakt.apps.event.AnomalyDetectedSource.TIME;
import static io.kontakt.apps.event.AnomalyDetectedSource.UNIT;

public class AnomalyGenerator {

    public static List<AnomalyEntity> generateAnomalies() {
        return Stream.of(
                builder().thermometerId("therm1").roomId("room1").detectionSource(UNIT).timePeriod(1).measurements(15d, 25d, 30d, 15d, 25d, 30d, 15d, 25d, 30d, 15d, 25d, 30d).build(),
                builder().thermometerId("therm2").roomId("room1").detectionSource(UNIT).timePeriod(2).measurements(15d, 25d, 30d).build(),
                builder().thermometerId("therm3").roomId("room2").detectionSource(TIME).timePeriod(1).measurements(15d, 25d, 30d, 15d).build()
        ).flatMap(Collection::stream).collect(Collectors.toList());
    }

    private static ThermometerIdBuilder builder() {
        return new AnomalyEntityBuilder();
    }

    private interface ThermometerIdBuilder {
        RoomIdBuilder thermometerId(String thermometerId);
    }

    private interface RoomIdBuilder {
        DetectionSourceBuilder roomId(String roomId);
    }

    private interface DetectionSourceBuilder {
        TimeBuilder detectionSource(AnomalyDetectedSource source);
    }

    private interface TimeBuilder {
        MeasurementBuilder timePeriod(int timePeriodInHours);
    }

    private interface MeasurementBuilder {
        AnomaliesBuilder measurements(Double... measurements);
    }

    private interface AnomaliesBuilder {
        List<AnomalyEntity> build();
    }


    private static class AnomalyEntityBuilder implements ThermometerIdBuilder, RoomIdBuilder, DetectionSourceBuilder, TimeBuilder, MeasurementBuilder, AnomaliesBuilder {

        private String thermometerId;
        private String roomId;
        private AnomalyDetectedSource source;
        private int timePeriodInSeconds;
        private List<Double> measurements;

        @Override
        public RoomIdBuilder thermometerId(String thermometerId) {
            this.thermometerId = thermometerId;
            return this;
        }

        @Override
        public DetectionSourceBuilder roomId(String roomId) {
            this.roomId = roomId;
            return this;
        }

        @Override
        public MeasurementBuilder timePeriod(int timePeriodInHours) {
            this.timePeriodInSeconds = timePeriodInHours * 3600 - 600;
            return this;
        }

        @Override
        public AnomaliesBuilder measurements(Double... measurements) {
            this.measurements = Arrays.stream(measurements).collect(Collectors.toList());
            return this;
        }

        @Override
        public List<AnomalyEntity> build() {
            List<AnomalyEntity> readings = new ArrayList<>();
            Instant time = Instant.now().minus(timePeriodInSeconds, ChronoUnit.SECONDS);
            long timestampDiff = (timePeriodInSeconds * 1000L) / measurements.size();
            for (Double temperature : measurements) {
                readings.add(new AnomalyEntity(UUID.randomUUID().toString(), new ThermometerEntity(thermometerId, roomId), new MeasurementEntity(temperature, time), source, Instant.now()));
                time = time.plus(timestampDiff, ChronoUnit.MILLIS);
            }
            return readings;
        }

        @Override
        public TimeBuilder detectionSource(AnomalyDetectedSource source) {
            this.source = source;
            return this;
        }
    }

}
