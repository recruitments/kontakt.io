package io.kontakt.apps.temperature.analytics.api;


import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.test.context.DynamicPropertyRegistry;
import org.springframework.test.context.DynamicPropertySource;
import org.testcontainers.containers.KafkaContainer;
import org.testcontainers.containers.MongoDBContainer;
import org.testcontainers.junit.jupiter.Testcontainers;
import org.testcontainers.utility.DockerImageName;

@SpringBootTest(classes = TemperatureAnalyticsApiApplication.class)
@Testcontainers
public class AbstractIntegrationTest {

    public final static KafkaContainer kafkaContainer;
    public final static MongoDBContainer mongodbContainer;

    @Autowired
    private MongoTemplate mongoTemplate;

    static {
        kafkaContainer = new KafkaContainer(DockerImageName.parse("confluentinc/cp-kafka:7.4.0"));
        kafkaContainer.start();

        mongodbContainer = new MongoDBContainer(DockerImageName.parse("mongo"));
        mongodbContainer.start();
    }

    @DynamicPropertySource
    static void datasourceConfig(DynamicPropertyRegistry registry) {
        registry.add("spring.cloud.stream.binders.kafka.environment.spring.cloud.stream.kafka.streams.binder.brokers", kafkaContainer::getBootstrapServers);
        registry.add("spring.data.mongodb.uri", mongodbContainer::getReplicaSetUrl);
    }

    @BeforeEach
    void setup() {
        var anomalies = AnomalyGenerator.generateAnomalies();
        anomalies.forEach(mongoTemplate::save);
    }

    @AfterEach
    void cleanup() {
        mongoTemplate.dropCollection("Anomaly");
    }

}
