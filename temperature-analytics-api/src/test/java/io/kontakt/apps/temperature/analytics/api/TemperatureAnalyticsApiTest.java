package io.kontakt.apps.temperature.analytics.api;

import io.kontakt.apps.event.ThermometerAnomalies;
import io.kontakt.apps.temperature.analytics.api.utils.AnomalyMapper;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.Arrays;

import static org.junit.jupiter.api.Assertions.*;

class TemperatureAnalyticsApiTest extends AbstractIntegrationTest {

    @Autowired
    private TemperatureAnalyticsApi temperatureAnalyticsApi;

    @Test
    public void checkIfReturnProperlyAnomaliesForRooms() {
        //given
        var firstRoom = "room1";
        var firstRoomAnomalies = AnomalyGenerator
                .generateAnomalies()
                .stream()
                .filter(anomaly -> anomaly.thermometer().roomId().equals(firstRoom))
                .map(AnomalyMapper::fromEntity)
                .toList();
        var secondRoom = "room2";
        var secondRoomAnomalies = AnomalyGenerator
                .generateAnomalies()
                .stream()
                .filter(anomaly -> anomaly.thermometer().roomId().equals(secondRoom))
                .map(AnomalyMapper::fromEntity)
                .toList();
        var nonExistingRoomAnomalies = "non_existing_room";

        //when
        var firstRoomAnomaliesActual = temperatureAnalyticsApi.getAllAnomaliesByRoomId(firstRoom);
        var secondRoomAnomaliesActual = temperatureAnalyticsApi.getAllAnomaliesByRoomId(secondRoom);
        var emptyAnomalies = temperatureAnalyticsApi.getAllAnomaliesByRoomId(nonExistingRoomAnomalies);

        //then
        assertEquals(firstRoomAnomalies.size(), firstRoomAnomaliesActual.size());
        assertEquals(secondRoomAnomalies.size(), secondRoomAnomaliesActual.size());
        assertTrue(emptyAnomalies.isEmpty());
    }

    @Test
    public void checkIfReturnProperlyAnomaliesForThermometers() {
        //given
        var firstThermometer = "therm1";
        var firstThermometerAnomalies = AnomalyGenerator
                .generateAnomalies()
                .stream()
                .filter(anomaly -> anomaly.thermometer().thermometerId().equals(firstThermometer))
                .map(AnomalyMapper::fromEntity)
                .toList();
        var secondThermometer = "therm2";
        var secondThermometerAnomalies = AnomalyGenerator
                .generateAnomalies()
                .stream()
                .filter(anomaly -> anomaly.thermometer().thermometerId().equals(secondThermometer))
                .map(AnomalyMapper::fromEntity)
                .toList();
        var nonExistingThermometerAnomalies = "non_existing_thermometer";

        //when
        var firstThermometerAnomaliesActual = temperatureAnalyticsApi.getAllAnomaliesByThermometerId(firstThermometer);
        var secondThermometerAnomaliesActual = temperatureAnalyticsApi.getAllAnomaliesByThermometerId(secondThermometer);
        var emptyAnomalies = temperatureAnalyticsApi.getAllAnomaliesByThermometerId(nonExistingThermometerAnomalies);

        //then
        assertEquals(firstThermometerAnomalies.size(), firstThermometerAnomaliesActual.size());
        assertEquals(secondThermometerAnomalies.size(), secondThermometerAnomaliesActual.size());
        assertTrue(emptyAnomalies.isEmpty());
    }

    @Test
    public void checkIfReturnedThermometersBasedOnAnomaliesCondition() {
        //given
        var minAnomalies = "3";
        var withinHours = "1";
        var expected = Arrays.asList(
                new ThermometerAnomalies("therm1", "room1", 12),
                new ThermometerAnomalies("therm3", "room2", 4)
        );

        //when
        var actual = temperatureAnalyticsApi.getAllThermometersWhichGeneratedTooManyAnomalies(minAnomalies, withinHours);

        //then
        assertEquals(2, actual.size());
        assertEquals(expected, actual);
    }

}