package io.kontakt.apps.temperature.analytics.api.consumer;

import io.kontakt.apps.event.*;
import io.kontakt.apps.temperature.analytics.api.AbstractIntegrationTest;
import io.kontakt.apps.temperature.analytics.api.TestKafkaProducer;
import io.kontakt.apps.temperature.analytics.api.storage.repository.AnomalyRepository;
import io.kontakt.apps.temperature.analytics.api.utils.AnomalyMapper;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.testcontainers.shaded.org.awaitility.Awaitility;

import java.time.Duration;
import java.time.Instant;
import java.util.Arrays;
import java.util.List;
import java.util.Objects;

import static io.kontakt.apps.event.AnomalyDetectedSource.TIME;
import static org.junit.jupiter.api.Assertions.assertFalse;

class AnomalyConsumerTest extends AbstractIntegrationTest {

    @Value("${spring.cloud.stream.bindings.anomalyStorageProcessor-in-0.destination}")
    private String inputTopic;

    @Autowired
    private AnomalyRepository anomalyRepository;

    @Test
    public void shouldConsumeAnomalyAndStoreInDb() {
        try(
                TestKafkaProducer<Anomalies> producer = new TestKafkaProducer<>(
                        kafkaContainer.getBootstrapServers(),
                        inputTopic
        )) {
            //prevalidate
            AnomalyDetected anomalyDetected = new AnomalyDetected(TIME, new Anomaly(1.0d, "room404", "therm404", Instant.now()));
            assertFalse(checkIfAnomalyExist(anomalyDetected));

            producer.produce(anomalyDetected.anomaly().thermometerId(), new Anomalies("therm404", List.of(anomalyDetected)));
            Awaitility
                    .await()
                    .atMost(Duration.ofSeconds(10))
                    .pollInterval(Duration.ofMillis(200))
                    .until(() -> checkIfAnomalyExist(anomalyDetected));
        }
    }

    private boolean checkIfAnomalyExist(AnomalyDetected anomalyDetected) {
        return anomalyRepository
                .findAll()
                .stream()
                .map(AnomalyMapper::fromEntity)
                .filter(anomaly -> Objects.equals(anomaly.anomaly().thermometerId(), anomalyDetected.anomaly().thermometerId())
                        && Objects.equals(anomaly.anomaly().roomId(), anomalyDetected.anomaly().roomId()))
                .count() == 1L;
    }

}