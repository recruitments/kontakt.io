package io.kontakt.apps.temperature.analytics.api.config;

import io.kontakt.apps.event.Anomalies;
import io.kontakt.apps.temperature.analytics.api.consumer.AnomalyConsumer;
import io.kontakt.apps.temperature.analytics.api.storage.repository.AnomalyRepository;
import org.apache.kafka.streams.kstream.KStream;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.util.function.Consumer;

@Configuration
public class KafkaConfig {

    @Bean
    public Consumer<KStream<String, Anomalies>> anomalyStorageProcessor(AnomalyRepository anomalyRepository) {
        return new AnomalyConsumer(anomalyRepository);
    }

}
