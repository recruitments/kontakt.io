package io.kontakt.apps.temperature.analytics.api;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.context.properties.ConfigurationPropertiesScan;
import org.springframework.data.mongodb.repository.config.EnableMongoRepositories;

@EnableMongoRepositories
@ConfigurationPropertiesScan
@SpringBootApplication
public class TemperatureAnalyticsApiApplication {

    public static void main(String[] args) {
        SpringApplication.run(TemperatureAnalyticsApiApplication.class, args);
    }

}
