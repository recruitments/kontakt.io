package io.kontakt.apps.temperature.analytics.api.storage.repository;

import io.kontakt.apps.temperature.analytics.api.storage.entity.AnomalyEntity;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.mongodb.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface AnomalyRepository extends MongoRepository<AnomalyEntity, String> {

    @Query("{ 'thermometer.roomId': '?0' }")
    List<AnomalyEntity> findAllByRoomId(String roomId);

    @Query("{ 'thermometer.thermometerId': '?0' }")
    List<AnomalyEntity> findAllByThermometerId(String thermometerId);

}
