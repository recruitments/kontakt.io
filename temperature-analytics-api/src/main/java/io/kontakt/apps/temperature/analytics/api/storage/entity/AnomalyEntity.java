package io.kontakt.apps.temperature.analytics.api.storage.entity;

import io.kontakt.apps.event.AnomalyDetectedSource;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import java.time.Instant;

@Document("Anomaly")
public record AnomalyEntity(

    @Id
    String id,
    ThermometerEntity thermometer,
    MeasurementEntity measurement,
    AnomalyDetectedSource anomalySource,
    Instant createdAt
) {}