package io.kontakt.apps.temperature.analytics.api.config;


import lombok.Builder;
import lombok.Getter;
import lombok.ToString;
import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.boot.context.properties.ConstructorBinding;

import java.util.Optional;

@Slf4j
@Builder
@ToString
@ConfigurationProperties(prefix = "thermometer.anomaly.detection")
@ConstructorBinding
public class ThermometerAnomalyDetectConfig {

    private static final Integer MIN_ANOMALIES = 10;
    private static final Integer WITHIN_LAST_HOURS_THERMOMETER_MEASUREMENTS = 1;
    public static final Integer WITHIN_LAST_HOURS_THERMOMETER_MEASUREMENTS_MAX = 24;

    @Getter private final Integer minAnomalies;
    @Getter private final Integer withinLastHoursThermometerMeasurements;

    public static ThermometerAnomalyDetectConfig defaultConfig() {
        return new ThermometerAnomalyDetectConfig(null, null);
    }

    public ThermometerAnomalyDetectConfig(
            Integer minAnomalies,
            Integer withinLastHoursThermometerMeasurements
    ) {
        withinLastHoursThermometerMeasurements = Optional
                .ofNullable(withinLastHoursThermometerMeasurements)
                .orElse(WITHIN_LAST_HOURS_THERMOMETER_MEASUREMENTS);
        this.minAnomalies = Optional.ofNullable(minAnomalies).orElse(MIN_ANOMALIES);
        this.withinLastHoursThermometerMeasurements =
                withinLastHoursThermometerMeasurements <= WITHIN_LAST_HOURS_THERMOMETER_MEASUREMENTS_MAX
                        ? withinLastHoursThermometerMeasurements
                        : WITHIN_LAST_HOURS_THERMOMETER_MEASUREMENTS_MAX;
        logConfig();
    }

    private void logConfig() {
        log.info("Using thermometer anomaly detect configuration: {}", this);
    }

}

