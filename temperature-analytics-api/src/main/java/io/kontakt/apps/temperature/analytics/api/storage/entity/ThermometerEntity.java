package io.kontakt.apps.temperature.analytics.api.storage.entity;

public record ThermometerEntity(String thermometerId, String roomId) {}
