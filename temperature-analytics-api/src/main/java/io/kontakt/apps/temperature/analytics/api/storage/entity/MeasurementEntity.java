package io.kontakt.apps.temperature.analytics.api.storage.entity;

import java.time.Instant;

public record MeasurementEntity(double temperature, Instant measurementTime) {
}
