package io.kontakt.apps.temperature.analytics.api;

import io.kontakt.apps.event.AnomalyDetected;
import io.kontakt.apps.event.ThermometerAnomalies;
import io.kontakt.apps.temperature.analytics.api.config.ThermometerAnomalyDetectConfig;
import io.kontakt.apps.temperature.analytics.api.dto.ThermometerAnomalyOccurrence;
import io.kontakt.apps.temperature.analytics.api.storage.AnomalyStorageService;
import io.kontakt.apps.temperature.analytics.api.utils.AnomalyMapper;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.stream.Collectors;

@RequiredArgsConstructor
@RequestMapping("/api")
@RestController
public class TemperatureAnalyticsApi {

    private final ThermometerAnomalyDetectConfig thermometerAnomalyDetectConfig;
    private final AnomalyStorageService anomalyStorageService;

    @GetMapping("/rooms/{roomId}/anomalies")
    public List<AnomalyDetected> getAllAnomaliesByRoomId(@PathVariable String roomId) {
        return anomalyStorageService
                .getAnomaliesByRoomId(roomId)
                .stream()
                .map(AnomalyMapper::fromEntity)
                .collect(Collectors.toList());
    }

    @GetMapping("/thermometers/{thermometerId}/anomalies")
    public List<AnomalyDetected> getAllAnomaliesByThermometerId(@PathVariable String thermometerId) {
        return anomalyStorageService
                .getAnomaliesByThermometerId(thermometerId)
                .stream()
                .map(AnomalyMapper::fromEntity)
                .collect(Collectors.toList());
    }

    @GetMapping("/thermometers/find-by-anomalies-occurrence")
    public List<ThermometerAnomalies> getAllThermometersWhichGeneratedTooManyAnomalies(
            @RequestParam(required = false) String minAnomalies,
            @RequestParam(required = false) String withinLastHoursThermometerMeasurements
    ) {
        return anomalyStorageService
                .getThermometersByAnomalyOccurrence(
                        ThermometerAnomalyOccurrence.fromUserRequestAndDefaultConfig(
                                minAnomalies,
                                withinLastHoursThermometerMeasurements,
                                thermometerAnomalyDetectConfig
                        )
                );
    }

}
