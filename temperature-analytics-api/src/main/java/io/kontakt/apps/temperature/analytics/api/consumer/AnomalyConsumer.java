package io.kontakt.apps.temperature.analytics.api.consumer;

import io.kontakt.apps.event.Anomalies;
import io.kontakt.apps.temperature.analytics.api.storage.repository.AnomalyRepository;
import io.kontakt.apps.temperature.analytics.api.utils.AnomalyMapper;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.apache.kafka.streams.kstream.KStream;

import java.util.function.Consumer;

@Slf4j
@RequiredArgsConstructor
public class AnomalyConsumer implements Consumer<KStream<String, Anomalies>> {

    private final AnomalyRepository anomalyRepository;

    @Override
    public void accept(KStream<String, Anomalies> events) {
        events
                .foreach(this::saveAnomalies);
    }

    private void saveAnomalies(String s, Anomalies anomalies) {
        if (anomalies.hasAnomalies()) {
            log.debug("Consumed anomalies {}", anomalies);
            var anomaliesEntity = anomalies
                    .anomalies()
                    .stream()
                    .map(AnomalyMapper::toEntity)
                    .toList();
            anomalyRepository.saveAll(anomaliesEntity);
        }
    }


}
