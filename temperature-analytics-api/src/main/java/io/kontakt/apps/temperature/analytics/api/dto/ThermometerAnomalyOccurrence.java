package io.kontakt.apps.temperature.analytics.api.dto;

import io.kontakt.apps.temperature.analytics.api.config.ThermometerAnomalyDetectConfig;

import java.util.Optional;

import static io.kontakt.apps.temperature.analytics.api.config.ThermometerAnomalyDetectConfig.WITHIN_LAST_HOURS_THERMOMETER_MEASUREMENTS_MAX;

public record ThermometerAnomalyOccurrence(
        int minAnomalies,
        int withinLastHoursThermometerMeasurements
) {

    public static ThermometerAnomalyOccurrence fromUserRequestAndDefaultConfig(
            String minAnomalies,
            String withinLastHoursThermometerMeasurements,
            ThermometerAnomalyDetectConfig thermometerAnomalyDetectConfig
    ) {
        int minAnomaliesValid = Optional
                .ofNullable(minAnomalies)
                .map(anomalies -> {
                    try {
                        return Integer.valueOf(anomalies);
                    } catch (Exception exception) {
                        return thermometerAnomalyDetectConfig.getMinAnomalies();
                    }
                })
                .orElse(thermometerAnomalyDetectConfig.getMinAnomalies());
        int withinLastHoursThermometerMeasurementsValid = Optional
                .ofNullable(withinLastHoursThermometerMeasurements)
                .map(within -> {
                    try {
                        int valid = Integer.valueOf(within);
                        return valid <= WITHIN_LAST_HOURS_THERMOMETER_MEASUREMENTS_MAX ? valid : WITHIN_LAST_HOURS_THERMOMETER_MEASUREMENTS_MAX;
                    } catch (Exception ex) {
                        return thermometerAnomalyDetectConfig.getWithinLastHoursThermometerMeasurements();
                    }
                })
                .orElse(thermometerAnomalyDetectConfig.getWithinLastHoursThermometerMeasurements());
        return new ThermometerAnomalyOccurrence(minAnomaliesValid, withinLastHoursThermometerMeasurementsValid);
    }

}
