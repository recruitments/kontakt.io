package io.kontakt.apps.temperature.analytics.api.utils;

import io.kontakt.apps.event.Anomaly;
import io.kontakt.apps.event.AnomalyDetected;
import io.kontakt.apps.temperature.analytics.api.storage.entity.AnomalyEntity;
import io.kontakt.apps.temperature.analytics.api.storage.entity.MeasurementEntity;
import io.kontakt.apps.temperature.analytics.api.storage.entity.ThermometerEntity;

import java.time.Instant;
import java.util.UUID;

public final class AnomalyMapper {

    public static AnomalyDetected fromEntity(AnomalyEntity anomalyEntity) {
        return new AnomalyDetected(
                anomalyEntity.anomalySource(),
                new Anomaly(
                    anomalyEntity.measurement().temperature(),
                    anomalyEntity.thermometer().roomId(),
                    anomalyEntity.thermometer().thermometerId(),
                    anomalyEntity.measurement().measurementTime()
                )
        );
    }

    public static AnomalyEntity toEntity(AnomalyDetected anomalyDetected) {
        return new AnomalyEntity(
                UUID.randomUUID().toString(),
                new ThermometerEntity(anomalyDetected.anomaly().thermometerId(), anomalyDetected.anomaly().roomId()),
                new MeasurementEntity(anomalyDetected.anomaly().temperature(), anomalyDetected.anomaly().timestamp()),
                anomalyDetected.source(),
                Instant.now()
        );
    }

}
