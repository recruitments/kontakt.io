package io.kontakt.apps.temperature.analytics.api.storage;

import io.kontakt.apps.event.ThermometerAnomalies;
import io.kontakt.apps.temperature.analytics.api.dto.ThermometerAnomalyOccurrence;
import io.kontakt.apps.temperature.analytics.api.storage.entity.AnomalyEntity;
import io.kontakt.apps.temperature.analytics.api.storage.repository.AnomalyRepository;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.domain.Sort;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.aggregation.*;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.stereotype.Service;

import java.time.Instant;
import java.util.List;

import static java.time.temporal.ChronoUnit.*;
import static org.springframework.data.mongodb.core.aggregation.Aggregation.group;
import static org.springframework.data.mongodb.core.aggregation.Aggregation.match;
import static org.springframework.data.mongodb.core.aggregation.Fields.field;
import static org.springframework.data.mongodb.core.aggregation.Fields.from;

@Slf4j
@RequiredArgsConstructor
@Service
public class AnomalyStorageService {

    private final AnomalyRepository anomalyRepository;

    private final MongoTemplate mongoTemplate;

    public List<AnomalyEntity> getAnomaliesByRoomId(String roomId) {
        return anomalyRepository.findAllByRoomId(roomId);
    }

    public List<AnomalyEntity> getAnomaliesByThermometerId(String thermometerId) {
        return anomalyRepository.findAllByThermometerId(thermometerId);
    }

    public List<ThermometerAnomalies> getThermometersByAnomalyOccurrence(ThermometerAnomalyOccurrence thermometerAnomalyOccurrence) {
        log.debug("Searching thermometer anomalies by criteria: {}", thermometerAnomalyOccurrence);
        MatchOperation lastWorkingHours = match(new Criteria("measurement.measurementTime").gte(Instant.now().minus(thermometerAnomalyOccurrence.withinLastHoursThermometerMeasurements(), HOURS)));
        GroupOperation groupByThermometerId = group(
                from(
                        field("thermometerId", "thermometer.thermometerId"),
                        field("roomId", "thermometer.roomId")
                )
        )
                .count().as("anomaliesDetected");
        ProjectionOperation project = Aggregation.project("anomaliesDetected", "thermometerId", "roomId");
        MatchOperation minAnomalies = match(new Criteria("anomaliesDetected").gte(thermometerAnomalyOccurrence.minAnomalies()));
        SortOperation sortByAnomalies = Aggregation.sort(Sort.by(Sort.Direction.DESC, "anomaliesDetected"));
        Aggregation aggregation = Aggregation.newAggregation(lastWorkingHours, groupByThermometerId, project, minAnomalies, sortByAnomalies);
        return mongoTemplate.aggregate(aggregation, "Anomaly", ThermometerAnomalies.class).getMappedResults();
    }

}